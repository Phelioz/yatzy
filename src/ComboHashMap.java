import java.util.HashMap;

import javax.naming.InitialContext;


public class ComboHashMap {

	public ComboHashMap(){
		init();
	}

	public void init() {
		makeComboHashMap();
	}

	public HashMap<String, Integer> makeComboHashMap(){
		HashMap<String, Integer> comboHashMap = new HashMap<String, Integer>();
		comboHashMap.put(Const.PAR, 0);
		comboHashMap.put(Const.TWO_PAR, 0);
		comboHashMap.put(Const.TRISS, 0);
		comboHashMap.put(Const.FYRTAL, 0);
		
		comboHashMap.put(Const.SMALL_STRAIGHT, 0);
		comboHashMap.put(Const.BIG_STRAIGHT, 0);
		
		comboHashMap.put(Const.KAK, 0);
		comboHashMap.put(Const.CHANS, 0);
		
		comboHashMap.put(Const.YATZY, 0);
		return comboHashMap;
	}
}
