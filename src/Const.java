
public class Const {
	// Names
	public static final String PAR = "Par";
	public static final String TWO_PAR = "Tv� Par";
	public static final String TRISS = "Triss";
	static final String FYRTAL = "Fyrtal";
	
	static final String SMALL_STRAIGHT = "Liten Stege";
	static final String BIG_STRAIGHT = "Stor Stege";
	
	static final String KAK = "K�k";
	static final String YATZY = "Yatzy";
	static final String CHANS = "Chans";
	
	// Points for different combos
	static final int MAX_NUMBER = 6;
	static final int MAX_THROWS = 5;
	static final int PAR_POINTS = 12;
	static final int TWO_PAR_POINTS = 22;
	static final int TRISS_POINTS = 18;
	static final int FYRTAL_POINTS = 24;
	static final int YATZY_POINTS = 50;
	static final int SMALL_STRAIGHT_POINTS = 15;
	static final int BIG_STRAIGHT_POINTS = 20;
	static final int KAK_POINTS = 28;
}
