import java.util.HashMap;
import java.util.Random;

public class YatzyRoll {

	int cubes[];
	int same[];
	int resultsBonus[];

	Random random;
	Results results;
	ComboHashMap comboHelper;

	public YatzyRoll() {
		comboHelper = new ComboHashMap();
		cubes = new int[Const.MAX_THROWS];
		random = new Random();
	}

	public Results roll() {

		cubes = rollCubes(cubes);
		same = countCubes(cubes);
		results = countPoints(same);

		return results;
	}

	private int[] rollCubes(int[] cubes) {
		for (int i = 0; i < cubes.length; i++) {
			int temp = random.nextInt(6);

			cubes[i] = temp + 1;

		}
		return cubes;
	}

	private int[] countCubes(int[] cubes) {
		int same[] = new int[Const.MAX_NUMBER];
		int count = 0;
		for (int i = 1; i <= Const.MAX_NUMBER; i++) {
			int points = 0;
			int combo = 0;

			for (int j = 0; j < cubes.length; j++) {
				if (cubes[j] == i) {
					combo += 1;
				}
			}
			same[count] = combo;
			count++;
		}
		return same;
	}

	private Results countPoints(int[] same) {
		Results results;
		results = protocol2(same);
		results.setResInt(protocol1(same));

		return results;
	}

	private int protocol1(int[] same) {
		int score = 0;
		int count = 1;
		for (int i = 0; i < same.length; i++) {
			score += same[i] * count;
			count++;
		}
		return score;
	}

	private Results protocol2(int[] same) {
		HashMap<String, Integer> resMap = comboHelper.makeComboHashMap();

		int score = 0;
		boolean hadPar = false;
		boolean hadTriss = false;
		boolean stair = false;
		int stairCount = 0;
		boolean smallStair = false;
		for (int i = 0; i < same.length; i++) {

			if (stair == false && i < 2) {
				if (same[i] > 0) {
					stairCount = i + 1;
					stair = true;
					if (i == 0)
						smallStair = true;
				}
			} else if (stair == true) {

				if (stairCount == 6 || (stairCount == 5 && smallStair == true)) {
					if (stairCount == 6) {
						score += Const.BIG_STRAIGHT_POINTS;
						resMap.put(Const.BIG_STRAIGHT,
								(resMap.get(Const.BIG_STRAIGHT) + 1));
					} else {
						score += Const.SMALL_STRAIGHT_POINTS;
						resMap.put(Const.SMALL_STRAIGHT,
								(resMap.get(Const.SMALL_STRAIGHT) + 1));
					}
					stair = false;
					smallStair = false;
				} else if (same[i] > 0) {
					stairCount++;
				} else {
					stair = false;
					smallStair = false;
					stairCount = 0;
				}
			}
			if (same[i] == 5) {
				score += Const.YATZY_POINTS;
				resMap.put(Const.YATZY, (resMap.get(Const.YATZY) + 1));
			}

			else if (same[i] == 4)
				score += Const.FYRTAL_POINTS;
			else if (same[i] == 3) {
				score += Const.TRISS_POINTS;
				hadTriss = true;
				resMap.put(Const.TRISS, (resMap.get(Const.TRISS) + 1));
			}

			else if (same[i] == 2 && hadPar == true) {
				score -= Const.PAR_POINTS;
				score += Const.TWO_PAR_POINTS;
				hadPar = false;
				resMap.put(Const.PAR, (resMap.get(Const.PAR) - 1));
				resMap.put(Const.TWO_PAR, (resMap.get(Const.TWO_PAR) + 1));
			} else if (same[i] == 2 && hadPar == false) {
				score += Const.PAR_POINTS;
				hadPar = true;
				resMap.put(Const.PAR, (resMap.get(Const.PAR) + 1));
			} else if ((hadPar && hadTriss) == true) {
				score -= Const.PAR_POINTS + Const.TRISS_POINTS;
				score += Const.KAK_POINTS;

				resMap.put(Const.PAR, (resMap.get(Const.PAR) - 1));
				resMap.put(Const.TRISS, (resMap.get(Const.TRISS) - 1));

				resMap.put(Const.KAK, (resMap.get(Const.KAK) + 1));
			}
		}
		Results results = new Results(0, score, resMap);
		return results;
	}

	public void add(Results roll) {
		this.results.addResults(roll);
	}

	public int[] getLastSame() {
		return same;
	}

	@Override
	public String toString() {
		return results.toString();
	}
}
