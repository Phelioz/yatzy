import java.util.HashMap;

// H�ller reda p� resultat
public class Results {

	int resInt;
	int resInt2;
	HashMap<String, Integer> resMap = new HashMap<String, Integer>();

	// G�r en tom resultat HashMap
	public Results() {
		int tempInt = 0;
		int tempInt2 = 0;

		HashMap<String, Integer> tempMap = new ComboHashMap()
				.makeComboHashMap();
		this.resInt2 = tempInt2;
		this.resMap = tempMap;

	}

	// G�r en resultat HashMap av det du f�r in
	public Results(int resInt, int resInt2, HashMap<String, Integer> resMap) {
		this.resInt = resInt;
		this.resInt2 = resInt2;
		this.resMap = resMap;

	}

	public int getResInt() {
		return resInt;
	}

	public void setResInt(int resInt) {
		this.resInt = resInt;
	}

	public HashMap<String, Integer> getResMap() {
		return resMap;
	}

	public void setResMap(HashMap<String, Integer> resMap) {
		this.resMap = resMap;
	}

	public int getResInt2() {
		return resInt2;
	}

	public void setResInt2(int resInt2) {
		this.resInt2 = resInt2;
	}

	public void addResults(Results results) {
		this.resInt += results.getResInt();
		this.resInt2 += results.getResInt2();

		for (String keyString : resMap.keySet()) {
			this.resMap.put(keyString, (resMap.get(keyString) + results
					.getResMap().get(keyString)));
		}
	}

	@Override
	public String toString() {
		return "Protokoll 1 �r: " + resInt + "\nProtokoll 2 �r: " + resInt2
				+ "\nKlarade kombinationer: " + resMap + "]";
	}

}
