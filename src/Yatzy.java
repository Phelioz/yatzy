import java.util.Scanner;

public class Yatzy {
	
	Scanner inputScanner;
	String inputString;
	YatzyRoll yatzyRoll;
	YatzyRoll tempYatzyRoll;
	String avslutaString = "avsluta";
	
	Results results;
	
	boolean first = true;
	int manyThrows;

	int tempLastSame[] = new int[Const.MAX_NUMBER]; 


	
	public Yatzy(){
		inputScanner = new Scanner(System.in);
		yatzyRoll = new YatzyRoll();
		yatzyRoll.roll();
		tempYatzyRoll = new YatzyRoll();
		results = new Results();
		manyThrows = 0;
		
		System.out.println("V�lkommen till Yatzy");
		
		// H�ller spelet rullande tills du v�ljer att trycka in avsluta
		while (true) {
			System.out.println("Skriv rulla f�r att rulla t�rningarna, Skriv avsluta f�r att avsluta ");
			inputString = inputScanner.next();
			if (!inputString.equalsIgnoreCase(avslutaString)){
					results.addResults(yatzyRoll.roll());
					tempLastSame = yatzyRoll.getLastSame();
					
					for (int i = 0; i < tempLastSame.length; i++) {
						System.out.println("Hur m�nga " + (i + 1) + "or: " + tempLastSame[i]);
					}	
					
					System.out.println(results);
					manyThrows++;
					if (manyThrows == 1) System.out.println("Du har kastat " + manyThrows + " g�ng\n");
					else System.out.println("Du har kastat " + manyThrows + " g�nger\n");
			}
			else break;
		}
	}
}
